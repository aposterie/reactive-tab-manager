(function () {
'use strict';

var windows;
(function (windows) {
    /** Gets all windows. */
    function getAll() {
        return new Promise(resolve => chrome.windows.getAll(resolve));
    }
    windows.getAll = getAll;
    /**
     * Gets the current window.
     * @since — Chrome 18.
     */
    function getCurrent(getInfo) {
        return new Promise(resolve => chrome.windows.getCurrent(getInfo, resolve));
    }
    windows.getCurrent = getCurrent;
})(windows || (windows = {}));
var tabs;
(function (tabs_1) {
    tabs_1._ = chrome.tabs;
    /**
     * Gets all tabs that have the specified properties, or all tabs if no
     * properties are specified.
     * @since — Chrome 16.
    */
    function query(queryInfo) {
        return new Promise(resolve => tabs_1._.query(queryInfo || {}, resolve));
    }
    tabs_1.query = query;
    /**
     * Gets first tab that have the specified properties, or of all tabs if no
     * properties are specified.
    */
    function queryOne(queryInfo) {
        return query(queryInfo).then(tabs => tabs[0]);
    }
    tabs_1.queryOne = queryOne;
    /** Retrieves details about the specified tab. */
    function get(tabId) {
        return new Promise(resolve => tabs_1._.get(tabId, resolve));
    }
    tabs_1.get = get;
    /** Gets the tab that this script call is being made from. May be undefined if called from a non-tab context (for example: a background page or popup view). */
    async function getCurrentTab() {
        return (await query({ active: true, currentWindow: true }))[0];
    }
    tabs_1.getCurrentTab = getCurrentTab;
    tabs_1.onCreated = tabs_1._.onCreated;
    tabs_1.onActivated = tabs_1._.onActivated;
    tabs_1.onUpdated = tabs_1._.onUpdated;
    tabs_1.onMoved = tabs_1._.onMoved;
    tabs_1.onRemoved = tabs_1._.onRemoved;
})(tabs || (tabs = {}));
const runtime = chrome.runtime;

const records = new Map();
function recordTab(tab) {
    if (!tab.id)
        throw new TypeError('Invalid type: ' + tab);
    records.set(JSON.stringify([tab.id, tab.sessionId]), { tab, lastOpened: new Date() });
}
(async function () {
    for (const window of await windows.getAll()) {
        for (const tab of await tabs.query({ windowId: window.id })) {
            recordTab(tab);
        }
    }
    tabs.onCreated.addListener(recordTab);
    tabs.onActivated.addListener(async (info) => {
        recordTab(await tabs.get(info.tabId));
    });
})();
runtime.onMessage.addListener((message, _sender, send) => {
    switch (message.type) {
        case 'LAST_OPENED':
            const record = records.get(JSON.stringify([message.tabId, message.sessionId]));
            if (!record) {
                send({ value: null });
            }
            else {
                send({ value: Number(record.lastOpened) });
            }
            break;
        default:
            throw new TypeError('Unknown message: ' + message);
    }
});
}).call(this);
