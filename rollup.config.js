import ts from 'rollup-typescript';
import node from 'rollup-plugin-node-resolve';
import babel from 'rollup-plugin-babel';
import cjs from 'rollup-plugin-commonjs';

if (!process.env.NODE_ENV) {
  process.env.NODE_ENV = 'production';
}

const build = (input, output) => ({
  input,
  output: {
    file: output,
    format: 'cjs',
    banner: '(function () {',
    footer: '}).call(this);',
  },
  context: 'self',
  plugins: [
    ts(),
    babel({
      // exclude: 'node_modules/**',
      comments: false,
      plugins: [
        'transform-inline-environment-variables',
        'minify-constant-folding',
        'minify-dead-code-elimination',
      ],
    }),
    node({
      module: true,
      extensions: ['.js', '.json', '.jsx'],
    }),
    cjs({
      namedExports: {
        react: 'Component createElement useCallback useState useEffect useMemo'.split(' '),
        'react-dom': ['render']
      }
    }),
  ]
})

export default [
  build('./src/index.tsx', 'dist/js/popup.js'),
  build('./src/background.ts', 'dist/js/background.js'),
]
