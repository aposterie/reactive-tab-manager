# reactive-tab-manager
Tab manager for Chrome.

## Build
```sh
# See `dist` folder.
yarn build
```

## License
[MIT License. For third party software used in this extension, see LICENSE](LICENSE).