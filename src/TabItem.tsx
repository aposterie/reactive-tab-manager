import React, { useMemo, useState, useEffect, useCallback } from 'react';
import { Icon } from './Icon';
import * as chrome from './chrome';
import { toFocus, toClose, toReload, toTogglePin } from './util';
import dayjs from 'dayjs';
import relativeTime from 'dayjs/plugin/relativeTime';

dayjs.extend(relativeTime);
dayjs.extend((o, c, d: any) => {
	d.en.relativeTime = {
    future: 'in %s',
    // past: '%s ago',
    past: '%s',
    s: 'a few secs',
    m: 'a min',
    mm: '%d mins',
    h: 'an hr',
    hh: '%d hrs',
    d: 'a day',
    dd: '%d days',
    M: 'a month',
    MM: '%d months',
    y: 'a year',
    yy: '%d years',
  }
})

export function TabItem({ tab }: { tab: Tab }) {
	const favicon = useMemo(() => (tab.favIconUrl || 'vendor/google/IDR_DEFAULT_FAVICON@2x')
		.replace(/^chrome:\/\/theme\//, 'vendor/google/'), [tab.favIconUrl])

	const [isCurrent, setCurrent] = useState(false)
	const [lastOpened, setLastOpened] = useState(0)

	const refresh = useCallback(() => {
		chrome.runtime.sendMessage({
			type: 'LAST_OPENED',
			tabId: tab.id,
			sessionId: tab.sessionId
		}, (response) => {
			setLastOpened(response.value);
		})

		chrome.tabs.getCurrentTab().then(t => setCurrent(t && t.id === tab.id))
	}, [tab.id, tab.url, tab.sessionId])

	useEffect(refresh, [tab.id, tab.sessionId])

	useEffect(() => {
		const interval = setInterval(refresh, 1000)
		return () => clearInterval(interval)
	}, [])

	const MAX_LENGTH = 48
	const title = useMemo(() =>
		tab.title!.length > MAX_LENGTH ? tab.title!.slice(0, MAX_LENGTH - 1) + '…' : tab.title,
		[tab.title]
	)

	const value = !isCurrent && lastOpened ? (dayjs(lastOpened) as any).fromNow() : 'S.o.'

	return (
		<div className="tab-entry">
			<span className="favicon">
				<img src={favicon} />
			</span>
			<span className="title" onClick={toFocus(tab)}>
				<span className="title-text">{ title }</span>
				<span className="last-opened">{ value }</span>
			</span>
			
			<div className="controls">
				<Icon className="times btn-outline-danger" onClick={toClose(tab)} />
				<Icon className={tab.pinned ? 'lock' : 'unlock-alt'} onClick={toTogglePin(tab)} />
				<Icon className="sync" onClick={toReload(tab)} />
			</div>
		</div>
	)
}
