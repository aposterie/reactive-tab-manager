import * as React from 'react';
import { TabItem } from './TabItem'

interface Props {
	filter: string
	tabs: Tab[];
}

export function TabList({ filter, tabs }: Props) {
	const childNodes = tabs
		.filter(({ url = '', title = '' }) => {
			if (filter.startsWith('/')) {
				// If the filter string ends in a slash, slice it out too.
				const regex = RegExp(filter.slice(1).replace(/\/$/, ''))
				return regex.test(url) || regex.test(title)
			} else {
				return url.includes(filter) || title.includes(filter)
			}
		})
		.map((tab, i) => <TabItem tab={tab} />);

	return <div className="tab-list">{ childNodes }</div>;
}