import * as React from 'react'
import { render } from 'react-dom'
import * as chrome from './chrome'
import { App } from './App'

chrome.windows.getCurrent({ populate: true }).then(({ tabs }) => {
  render(
    <App initialTabs={ tabs! } />,
    document.getElementById('react-container')!
  );
});
