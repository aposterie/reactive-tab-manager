import * as chrome from "./chrome";

interface TabRecord {
  tab: Tab;
  lastOpened: Date;
}

const records = new Map<string, TabRecord>();

function recordTab(tab: Tab) {
  if (!tab.id) throw new TypeError('Invalid type: ' + tab);
  records.set(JSON.stringify([tab.id, tab.sessionId]), { tab, lastOpened: new Date() });
}

(async function () {
  for (const window of await chrome.windows.getAll()) {
    for (const tab of await chrome.tabs.query({ windowId: window.id })) {
      recordTab(tab);
    }
  }
  chrome.tabs.onCreated.addListener(recordTab);
  chrome.tabs.onActivated.addListener(async info => {
    recordTab(await chrome.tabs.get(info.tabId))
  });
})();

chrome.runtime.onMessage.addListener((message, _sender, send) => {
  switch (message.type) {
    case 'LAST_OPENED':
      const record = records.get(JSON.stringify([message.tabId, message.sessionId]));
      if (!record) {
        send({ value: null });
      } else {
        send({ value: Number(record.lastOpened) });
      }
      break;
    default:
      throw new TypeError('Unknown message: ' + message);
  }
});
