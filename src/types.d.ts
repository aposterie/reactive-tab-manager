type Tab = chrome.tabs.Tab

type Transform<T, R> = (value: T) => R
type Consumer<T> = (value: T) => void
type huh<T> = T | undefined

declare module 'dayjs/plugin/relativeTime';