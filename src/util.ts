const to = <K, R>(fn: Transform<K, R>) => (value: K) => () => fn(value);
export const acceptArray = <K, R>(fn: Transform<K, R>) => (values: K[]) => values.forEach(fn);

const ifTabIDPresent = (fn: Consumer</* tabId */number>) =>
  (tab: Tab) => { tab.id != null && fn(tab.id) }

export const closeTab = ifTabIDPresent(chrome.tabs.remove);
export const reloadTab = ifTabIDPresent(chrome.tabs.reload);
export const focusTab = ifTabIDPresent(
  id => chrome.tabs.update(id, { highlighted: true, active: true })
);
const togglePin = (tab: Tab) =>
  tab.id != null && chrome.tabs.update(tab.id, { pinned: !tab.pinned });

export const toClose = to(closeTab);
export const toReload = to(reloadTab);
export const toFocus = to(focusTab);
export const toTogglePin = to(togglePin);

export const toCloseTabs = to(acceptArray(closeTab));
export const toReloadTabs = to(acceptArray(closeTab));
