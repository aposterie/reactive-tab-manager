import * as React from 'react';

export function Icon(p: { className: string, onClick?(): void, style?: any }) {
	return <i className={`fa fa-${p.className}`} onClick={ p.onClick } style={ p.style } />;
}
