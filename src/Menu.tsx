import React, { useCallback, useState } from 'react';
import { Icon } from './Icon';
import { closeTab, reloadTab, toCloseTabs, toReloadTabs } from './util';

const MenuItem = (p: { onClick(): void, children?: React.ReactNode }) =>
	<a onClick={p.onClick} className="dropdown-item">{p.children}</a>;

const MenuSeparator = <li role="separator" className="divider" />

const newTab = () =>
	chrome.tabs.create({ url: 'about:blank' })

export function Menu({ tabs }: { tabs: Tab[] }) {
	const [visible, setVisible] = useState(false)
	
	const closeDuplicates = useCallback(() => {
		const urls = new Set<string | undefined>()
		tabs.forEach(tab => {
			urls.has(tab.url) ? closeTab(tab) : urls.add(tab.url)
		});
	}, [tabs])

	const closeGoogle = useCallback(() => {
		tabs.filter((tab) =>
			tab.title!.includes('Google') &&
			/^https:\/\/www\.google\.(...?|co\...)\//.test(tab.url!)
		).forEach(closeTab);
	}, [tabs])

	const closeWikipedia = useCallback(() => {
		tabs.filter((tab) =>
			/^https?:\/\/[^\.]+\.wikipedia\.org/.test(tab.url!)
		).forEach(closeTab);
	}, [tabs])

	const toggleVisibility = useCallback((e: React.MouseEvent) => {
		e.preventDefault();
		setVisible(v => !v)
	}, []);

	return (
		<span className="dropdown">
			<a onClick={toggleVisibility} className="dropdown-toggle">
				<Icon className="cog" />
			</a>
			<ul className={`dropdown-menu dropdown-menu-right ${visible ? 'show' : ''}`} role="menu">
				<MenuItem onClick={ toCloseTabs(tabs) }>Close All Tabs</MenuItem>
				<MenuItem onClick={ closeDuplicates }>Close Duplicate Tabs</MenuItem>
				<MenuItem onClick={ closeGoogle }>Close Google Search Tabs</MenuItem>
				<MenuItem onClick={ closeWikipedia }>Close Wikipedia Tabs</MenuItem>
				{ MenuSeparator }
				<MenuItem onClick={ toReloadTabs(tabs) }>Refresh All</MenuItem>
				<MenuItem onClick={ newTab }>Create New Tab</MenuItem>
			</ul>
		</span>
	);
}
