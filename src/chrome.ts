export namespace windows {
  /** Gets all windows. */
  export function getAll(): Promise<chrome.windows.Window[]> {
    return new Promise(resolve => chrome.windows.getAll(resolve));
  }

  /**
   * Gets the current window.  
   * @since — Chrome 18.
   */
  export function getCurrent(getInfo: chrome.windows.GetInfo): Promise<chrome.windows.Window> {
    return new Promise(resolve => chrome.windows.getCurrent(getInfo, resolve))
  }
}

export namespace tabs {
  export const _ = chrome.tabs;

  /**
   * Gets all tabs that have the specified properties, or all tabs if no
   * properties are specified.
   * @since — Chrome 16.
  */
  export function query(queryInfo?: chrome.tabs.QueryInfo): Promise<Tab[]> {
    return new Promise(resolve => _.query(queryInfo || {}, resolve));
  }

  /**
   * Gets first tab that have the specified properties, or of all tabs if no
   * properties are specified.
  */
  export function queryOne(queryInfo: chrome.tabs.QueryInfo): Promise<Tab> {
    return query(queryInfo).then(tabs => tabs[0]);
  }
  /** Retrieves details about the specified tab. */
  export function get(tabId: number): Promise<Tab> {
    return new Promise(resolve => _.get(tabId, resolve));
  }

  /** Gets the tab that this script call is being made from. May be undefined if called from a non-tab context (for example: a background page or popup view). */
  export async function getCurrentTab(): Promise<Tab> {
    return (await query({ active: true, currentWindow: true }))[0]
  }

  export const onCreated = _.onCreated;
  export const onActivated = _.onActivated;
  export const onUpdated = _.onUpdated;
  export const onMoved = _.onMoved;
  export const onRemoved = _.onRemoved;
}

export const runtime = chrome.runtime;

export const _ = chrome;
