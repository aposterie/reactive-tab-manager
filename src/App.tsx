import React, { useState, useEffect, useCallback } from 'react';
import * as chrome from './chrome';
import { Menu } from './Menu';
import { TabList } from './TabList';

interface Props {
  initialTabs: Tab[]
}

export function App(props: Props) {
  const [tabs, setTabs] = useState<Tab[]>(props.initialTabs)
  const [searchQuery, setSearchQuery] = useState('')

  useEffect(() => {
    chrome.tabs.onCreated.addListener(update);
    chrome.tabs.onUpdated.addListener(update);
    chrome.tabs.onMoved.addListener(update);
    chrome.tabs.onRemoved.addListener(update);
  }, [])

  const update = useCallback(async () => {
    const window = await chrome.windows.getCurrent({ populate: true });
    setTabs(window.tabs || []);
  }, [])

  const updateSearch = useCallback((e: React.KeyboardEvent<HTMLInputElement>) => {
    setSearchQuery(e.currentTarget.value);
  }, []);

  return (
    <div>
      <header>
        <input
          className="form-control filter"
          onKeyUp={ updateSearch }
          placeholder="Rechercher"
        />
        <Menu tabs={ tabs } />
      </header>
      <TabList tabs={ tabs } filter={ searchQuery } />
    </div>
  );
}